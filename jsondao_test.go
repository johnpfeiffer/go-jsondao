package jsondao

// go test
// go test -v -run TestParsingNestedAndVaried
// go test -cover

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
)

var dataExampleURLJSON = []byte(`
{
   "version": 1,
   "checksum": "c2f635678cd7c6b7adc5fc6789a333a9",
   "roots": {
      "bookmark_bar": {
         "children": [ {
			"date_added": "13018334978867413",
            "id": "191",
            "name": "Canonical URL Example",
            "type": "url",
            "url": "https://example.com"
		 } ],
         "date_added": "12963388334079025",
         "date_modified": "13091341837695981",
         "id": "1",
         "name": "Bookmarks bar",
         "type": "folder"
      }
	}
}
`)

type toMapTests struct {
	k               string
	expectedString  string
	expectedInt     int
	expectedFloat64 float64
}

func checkForUnexpectedError(t *testing.T, err error) {
	if err != nil {
		t.Error("\nUnexpected error:", err)
	}
}
func TestToMap(t *testing.T) {
	m, err := ToMap(dataExampleURLJSON)
	checkForUnexpectedError(t, err)
	if m["checksum"] != "c2f635678cd7c6b7adc5fc6789a333a9" {
		t.Error("\nExpected c2f635678cd7c6b7adc5fc6789a333a9 but received:", m["checksum"])
	}
	var expected float64 = 1
	if m["version"] != expected {
		t.Error("\nExpected 1 (float64) but received:", m["version"])
	}
	o := m["roots"]
	// https://golang.org/pkg/reflect/#Kind
	fmt.Println(reflect.TypeOf(o))
	fmt.Println(reflect.TypeOf(o).Kind())
	if reflect.TypeOf(o).Kind() != reflect.Map {
		t.Error("\nExpected roots as a Map but received:", reflect.TypeOf(o).Kind())
	}
}

func TestToMapOfRawMessages(t *testing.T) {
	m, err := ToMapOfRawMessages(dataExampleURLJSON)
	checkForUnexpectedError(t, err)
	var checksum string
	err = json.Unmarshal(m["checksum"], &checksum)
	checkForUnexpectedError(t, err)
	if checksum != "c2f635678cd7c6b7adc5fc6789a333a9" {
		t.Error("\nExpected c2f635678cd7c6b7adc5fc6789a333a9 but received:", m["checksum"])
	}
	var version float64 = 1
	err = json.Unmarshal(m["version"], &version)
	checkForUnexpectedError(t, err)
	if version != 1 {
		t.Error("\nExpected 1 (float64) but received:", m["version"])
	}

	// Clearly this level of raw unpackaging is unnecessary if the expected structure is known
	// AND there are guarantees preventing bugs/corruption of the messages
	// AND there is not some performance reason to only want to parse the top level and "peek"
	r, err := ToMapOfRawMessages(m["roots"])
	checkForUnexpectedError(t, err)
	b, err := ToMapOfRawMessages(r["bookmark_bar"])
	checkForUnexpectedError(t, err)

	var name string
	err = json.Unmarshal(b["name"], &name)
	checkForUnexpectedError(t, err)
	if name != "Bookmarks bar" {
		t.Error("\nExpected name 'Bookmarks bar' but received:", b["name"])
	}
	var bType string
	err = json.Unmarshal(b["type"], &bType)
	checkForUnexpectedError(t, err)
	if bType != "folder" {
		t.Error("\nExpected type 'folder' but received:", b["type"])
	}
	var dateAdded string
	err = json.Unmarshal(b["date_added"], &dateAdded)
	checkForUnexpectedError(t, err)
	if dateAdded != "12963388334079025" {
		t.Error("\nExpected 12963388334079025 but received:", b["date_added"])
	}
	var dateModified string
	err = json.Unmarshal(b["date_modified"], &dateModified)
	checkForUnexpectedError(t, err)
	if dateModified != "13091341837695981" {
		t.Error("\nExpected 13091341837695981 but received:", b["date_modified"])
	}
	var id string
	err = json.Unmarshal(b["id"], &id)
	checkForUnexpectedError(t, err)
	if id != "1" {
		t.Error("\nExpected id '1' but received:", b["id"])
	}

}

func TestToMapOfStringsAndRaw(t *testing.T) {
	topLevel, err := ToMapOfStringsAndRaw(dataExampleURLJSON, map[string]bool{"checksum": true})
	checkForUnexpectedError(t, err)
	if topLevel["checksum"].s != "c2f635678cd7c6b7adc5fc6789a333a9" {
		t.Error("\nExpected c2f635678cd7c6b7adc5fc6789a333a9 but received:", topLevel["checksum"])
	}

	roots, err := ToMapOfRawMessages(topLevel["roots"].raw)
	checkForUnexpectedError(t, err)

	b, err := ToMapOfStringsAndRaw(roots["bookmark_bar"],
		map[string]bool{
			"name":          true,
			"type":          true,
			"date_added":    true,
			"date_modified": true,
			"id":            true,
		})
	checkForUnexpectedError(t, err)
	// probably blows up before this but "children" has to be accounted for
	if len(b) != 6 {
		t.Error("\nExpected 6 items but received:", len(b))
		for k := range b {
			fmt.Printf(" %s ", k)
		}
	}
	if b["name"].s != "Bookmarks bar" {
		t.Error("\nExpected name 'Bookmarks bar' but received:", b["name"].s)
	}
	if b["type"].s != "folder" {
		t.Error("\nExpected type 'folder' but received:", b["type"].s)
	}
	if b["date_added"].s != "12963388334079025" {
		t.Error("\nExpected 12963388334079025 but received:", b["date_added"].s)
	}
	if b["date_modified"].s != "13091341837695981" {
		t.Error("\nExpected 13091341837695981 but received:", b["date_modified"].s)
	}
	if b["id"].s != "1" {
		t.Error("\nExpected id '1' but received:", b["id"].s)
	}
}

// TODO: test for errors in json parsing
