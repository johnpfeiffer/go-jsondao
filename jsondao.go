package jsondao

// Parsing unknown or unverified JSON is more common than fully perfect schema mapping to types/structs/objects
// jsondao provides some helpful wrappers using the empty interface or defers using https://golang.org/pkg/encoding/json/#RawMessage
// https://blog.golang.org/json-and-go

import (
	"encoding/json"
	"io/ioutil"
)

// FileToMap is a helper function that combines ReadFile and ToMap()
func FileToMap(filepath string) (map[string]interface{}, error) {
	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		return nil, err
	}
	dataMap, err := ToMap(data)
	return dataMap, err
}

// ToMap assumes top level keys are strings (i.e. NOT a json file with just []) and returns the bytes as a Map of objects
func ToMap(data []byte) (map[string]interface{}, error) {
	// A map of string to any type https://blog.golang.org/laws-of-reflection , http://research.swtch.com/interfaces
	var datamap map[string]interface{}
	err := json.Unmarshal(data, &datamap)
	return datamap, err
}

// FileToMapOfRawMessages is a helper function that combines ReadFile and ToMapOfRawMessages
func FileToMapOfRawMessages(filepath string) (map[string]json.RawMessage, error) {
	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		return nil, err
	}
	m, err := ToMapOfRawMessages(data)
	return m, err
}

// ToMapOfRawMessages defers processing beyond the initial map of key strings https://golang.org/pkg/encoding/json/#RawMessage
func ToMapOfRawMessages(data []byte) (map[string]json.RawMessage, error) {
	var datamap map[string]json.RawMessage
	err := json.Unmarshal(data, &datamap)
	return datamap, err
}

// StringsAndRawMessages is a hybrid type for partial json processing to get string values out
type StringsAndRawMessages struct {
	raw json.RawMessage
	s   string
}

// ToMapOfStringsAndRaw defers processing beyond the top keys and stringKeys that are definitely known to be just a string value
func ToMapOfStringsAndRaw(data []byte, stringKeys map[string]bool) (map[string]StringsAndRawMessages, error) {
	temp, err := ToMapOfRawMessages(data)
	if err != nil {
		return nil, err
	}

	m := make(map[string]StringsAndRawMessages)
	for k, v := range temp {
		hybrid := StringsAndRawMessages{raw: v}
		_, ok := stringKeys[k]
		if ok {
			err = json.Unmarshal(v, &hybrid.s)
			if err != nil {
				return m, err
			}
		}
		m[k] = hybrid
	}
	return m, err
}

// Write outputs the data as formatted JSON to the file path
func Write(data interface{}, path string) error {
	// TODO: allow permissions to be overwritten
	theJSON, _ := json.MarshalIndent(data, "", "  ")
	err := ioutil.WriteFile(path, theJSON, 0644)
	return err
}
